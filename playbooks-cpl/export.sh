#!/bin/bash

vboxmanage storageattach 'cpl-vm' --storagectl SCSI --port 1 --medium none
vboxmanage sharedfolder remove 'cpl-vm' --name vagrant
vboxmanage export 'cpl-vm' --ovf10 -o pm-vm-1.2.ova

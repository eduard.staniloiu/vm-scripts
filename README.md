# VM creation tutorial

## Setting up the environment

Tested on Linux Mint 19 (codename Tara, based on Ubuntu 18.04).
Any reasonable recent Linux distribution should work.

    $ sudo apt-get install vagrant virtualbox virtualbox-qt git-lfs python-pip python-setuptools
    $ pip install ansible
    $ vagrant plugin install vagrant-disksize
    $ vagrant plugin install vagrant-reload

## Building a VM

    $ git clone git@gitlab.com:adrian.stratulat/vm-scripts.git
    $ cd vagrantfile-pm
    $ vagrant up

Running the `vagrant up` command will create and configure the VM.

